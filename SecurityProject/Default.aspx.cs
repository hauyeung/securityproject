﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace SecurityProject
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.IsAuthenticated) 
            { 
                WelcomeBackMessage.Text = "You are Logged In, " + User.Identity.Name; 
                LoggedInMessagePanel.Visible = true; 
                NotLoggedInMessagePanel.Visible = false; 
            } 
            else 
            { 
                LoggedInMessagePanel.Visible = false; 
                NotLoggedInMessagePanel.Visible = true; 
            }
        

            if (Membership.GetUser("Admin") == null)
            {
                Membership.CreateUser("Admin", "Admin123");
            }
            if (Membership.GetUser("Member") == null)
            {
                Membership.CreateUser("Member", "Member123");
            }
            if (!Roles.RoleExists("Admin"))
            {
                Roles.CreateRole("Admin");
                Roles.AddUserToRole("Admin", "Admin");
            }

            if (!Roles.RoleExists("Member"))
            {
                Roles.CreateRole("Member");
                Roles.AddUserToRole("Member", "Member");
            }
            

        }
    }
}
