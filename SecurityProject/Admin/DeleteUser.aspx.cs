﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace SecurityProject.Admin
{
    public partial class DeleteUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                DisplayRolesInGrid();

        }

        protected void CurrentUsersGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Label lblRoleName = CurrentUsersGrid.Rows[e.RowIndex].FindControl("UserNameLabel") as Label;
            Membership.DeleteUser(lblRoleName.Text);
            DisplayRolesInGrid();
        }

        private void DisplayRolesInGrid()
        {
            CurrentUsersGrid.DataSource = Membership.GetAllUsers();
            CurrentUsersGrid.DataBind();
        }
    }
}