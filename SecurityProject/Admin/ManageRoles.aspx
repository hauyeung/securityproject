﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageRoles.aspx.cs" Inherits="SecurityProject.Admin.ManageRoles" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <asp:Panel ID="RoleGridPanel" runat="server">
        <b>Current Roles</b>
        <asp:GridView ID="CurrentRolesGrid" runat="server" AutoGenerateColumns="false" OnRowDeleting="CurrentRolesGrid_RowDeleting">
            <Columns>
                <asp:TemplateField HeaderText="Role">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="RoleNameLabel" Text='<%# Container.DataItem %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField DeleteText="Delete" ShowDeleteButton="True" />
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <br />
    <asp:Panel ID="CreateRolePanel" runat="server">
        <b>Create a New Role:</b>
        <asp:TextBox ID="RoleNameTextBox" runat="server"></asp:TextBox>
        <asp:Button ID="CreateRoleButton" runat="server" Text="Create Role" OnClick="CreateRoleButton_Click" />
    </asp:Panel>

</asp:Content>
