﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateUser.aspx.cs" Inherits="SecurityProject.Admin.CreateUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<asp:Panel ID="CreateUserWizardPanel" runat="server">
        <asp:CreateUserWizard ID="CreateUserWithRoles" runat="server" ContinueDestinationPageUrl="~/Default.aspx"
            LoginCreatedUser="False" OnActiveStepChanged="CreateUserWithRoles_ActiveStepChanged">
            <WizardSteps>
                <asp:CreateUserWizardStep ID="CreateUserWithRolesWizardStep" runat="server">
                </asp:CreateUserWizardStep>
                <asp:WizardStep ID="RoleStep" runat="server" StepType="Step" Title="Roles" AllowReturn="False">
                    <asp:CheckBoxList ID="RoleCheckBoxList" runat="server">
                    </asp:CheckBoxList>
                </asp:WizardStep>
                <asp:CompleteWizardStep ID="CompleteWizardStep" runat="server">
                </asp:CompleteWizardStep>
            </WizardSteps>
        </asp:CreateUserWizard>
    </asp:Panel>

</asp:Content>
