﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace SecurityProject.Admin
{
    public partial class ManageUsersWithRoles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!Page.IsPostBack)
            {
                BindUsersToUserList();
                BindRolesToList();
                CheckForUserRoleSelection();
                UserIsInRole();
            }

        }

        private void BindUsersToUserList()
        {
            MembershipUserCollection users = Membership.GetAllUsers();
            UserListDropDown.DataSource = users;
            UserListDropDown.DataBind();
        }

        private void BindRolesToList()
        {
            string[] roles = Roles.GetAllRoles();
            UsersRoleList.DataSource = roles;
            UsersRoleList.DataBind();
            RoleDropDownList.DataSource = roles;
            RoleDropDownList.DataBind();
        }

        private void CheckForUserRoleSelection()
        {
            string userNameSelection = UserListDropDown.SelectedValue;
            string[] userRolesSelection = Roles.GetRolesForUser(userNameSelection);
            foreach (RepeaterItem item in UsersRoleList.Items)
            {
                CheckBox RoleCheckBox = item.FindControl("RoleCheckBox") as CheckBox;
                if (userRolesSelection.Contains<string>(RoleCheckBox.Text))
                    RoleCheckBox.Checked = true;
                else
                    RoleCheckBox.Checked = false;
            }
        }

        protected void UserDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckForUserRoleSelection();
        }

        protected void RoleCheckBox_CheckChanged(object sender, EventArgs e)
        {
            CheckBox RoleCheckBox = sender as CheckBox;
            string selectedUser = UserListDropDown.SelectedValue;
            string roleName = RoleCheckBox.Text;
            if (RoleCheckBox.Checked)
                Roles.AddUserToRole(selectedUser, roleName);
            else
                Roles.RemoveUserFromRole(selectedUser, roleName);
            UserIsInRole();
        }

        private void UserIsInRole()
        {
            string selectedRoleName = RoleDropDownList.SelectedValue;
            string[] userInRole = Roles.GetUsersInRole(selectedRoleName);
            RolesUserGrid.DataSource = userInRole;
            RolesUserGrid.DataBind();
        }

        protected void RoleDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            UserIsInRole();
        }

        protected void RolesUserGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string selectedRoleName = RoleDropDownList.SelectedValue;
            Label UserNameLabel = RolesUserGrid.Rows[e.RowIndex].FindControl("UserNameLabel") as Label;
            Roles.RemoveUserFromRole(UserNameLabel.Text, selectedRoleName);
            UserIsInRole();
            CheckForUserRoleSelection();
        }

        protected void AddUserToRoleButton_Click(object sender, EventArgs e)
        {
            string selectedRoleName = RoleDropDownList.SelectedValue;
            string userNameToAddToRole = AddUserToRole.Text;
            if (userNameToAddToRole.Trim().Length == 0)
            {
                return;
            }
            MembershipUser userInfo = Membership.GetUser(userNameToAddToRole);
            if (userInfo == null)
            {
                return;
            }
            if (Roles.IsUserInRole(userNameToAddToRole, selectedRoleName))
            {
                return;
            }
            Roles.AddUserToRole(userNameToAddToRole, selectedRoleName);
            AddUserToRole.Text = string.Empty;
            UserIsInRole();
            CheckForUserRoleSelection();
        }

    }
}