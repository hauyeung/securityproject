﻿<%@ Page  Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="DeleteUser.aspx.cs" Inherits="SecurityProject.Admin.DeleteUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <asp:Panel ID="RoleGridPanel" runat="server">
        <b>Current Users</b>
        <asp:GridView ID="CurrentUsersGrid" runat="server" AutoGenerateColumns="false" OnRowDeleting="CurrentUsersGrid_RowDeleting">
            <Columns>
                <asp:TemplateField HeaderText="User">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="UserNameLabel" Text='<%# Container.DataItem %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField DeleteText="Delete" ShowDeleteButton="True" />
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <br />


</asp:Content>
