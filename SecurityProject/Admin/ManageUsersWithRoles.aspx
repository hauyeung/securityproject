﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageUsersWithRoles.aspx.cs" Inherits="SecurityProject.Admin.ManageUsersWithRoles" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<asp:Panel ID="UserListDropDownPanel" runat="server">
        <b>Select User:</b>
        <asp:DropDownList ID="UserListDropDown" runat="server" AutoPostBack="True" DataTextField="UserName"
            DataValueField="UserName" OnSelectedIndexChanged="UserDropDownList_SelectedIndexChanged">
        </asp:DropDownList>
    </asp:Panel>
    <br />
    <asp:Panel ID="UsersRoleListPanel" runat="server">
        <asp:Repeater ID="UsersRoleList" runat="server">
            <ItemTemplate>
                <asp:CheckBox runat="server" ID="RoleCheckBox" AutoPostBack="true" Text='<%# Container.DataItem %>'
                    OnCheckedChanged="RoleCheckBox_CheckChanged" />
                <br />
            </ItemTemplate>
        </asp:Repeater>
    </asp:Panel>
    <br />
    <asp:Panel ID="RoleListDropDownPanel" runat="server">
    </asp:Panel>
    <asp:DropDownList ID="RoleDropDownList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="RoleDropDownList_SelectedIndexChanged">
    </asp:DropDownList>
    <br />
    <asp:Panel ID="RolesUserGridPanel" runat="server">
        <asp:GridView ID="RolesUserGrid" runat="server" AutoGenerateColumns="false" EmptyDataText="There are no users in this role."
            OnRowDeleting="RolesUserGrid_RowDeleting">
            <Columns>
                <asp:TemplateField HeaderText="Users">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="UserNameLabel" Text='<%# Container.DataItem %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <br />
    <asp:Panel ID="AddUserToRolePanel" runat="server">
    <b>UserName:</b>
    <asp:TextBox ID="AddUserToRole" runat="server"></asp:TextBox>
    <asp:Button ID="AddUserToRoleButton" runat="server" Text="Add User to Role" OnClick="AddUserToRoleButton_Click" />
    </asp:Panel>

</asp:Content>
