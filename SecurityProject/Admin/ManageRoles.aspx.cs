﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace SecurityProject.Admin
{
    public partial class ManageRoles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                DisplayRolesInGrid();

        }

        protected void CreateRoleButton_Click(object sender, EventArgs e)
        {
            string newRoleName = RoleNameTextBox.Text.Trim();
            if (!Roles.RoleExists(newRoleName))
            {
                Roles.CreateRole(newRoleName);
                DisplayRolesInGrid();
            }
            RoleNameTextBox.Text = string.Empty;
        }

        private void DisplayRolesInGrid()
        {
            CurrentRolesGrid.DataSource = Roles.GetAllRoles();
            CurrentRolesGrid.DataBind();
        }

        protected void CurrentRolesGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Label lblRoleName = CurrentRolesGrid.Rows[e.RowIndex].FindControl("RoleNameLabel") as Label;
            Roles.DeleteRole(lblRoleName.Text, false);
            DisplayRolesInGrid();
        }

    }
}