﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace SecurityProject.Admin
{
    public partial class CreateUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                WizardStep RolesStep = CreateUserWithRoles.FindControl("RoleStep") as WizardStep;
                CheckBoxList RoleList = RolesStep.FindControl("RoleCheckBoxList") as CheckBoxList;
                RoleList.DataSource = Roles.GetAllRoles();
                RoleList.DataBind();
            }

        }

        protected void CreateUserWithRoles_ActiveStepChanged(object sender, EventArgs e)
        {
            if (CreateUserWithRoles.ActiveStep.Title == "Complete")
            {
                WizardStep RoleStep = CreateUserWithRoles.FindControl("RoleStep") as WizardStep;
                CheckBoxList RoleCheckBoxList = RoleStep.FindControl("RoleCheckBoxList") as CheckBoxList;
                foreach (ListItem item in RoleCheckBoxList.Items)
                {
                    if (item.Selected)
                        Roles.AddUserToRole(CreateUserWithRoles.UserName, item.Text);
                }
            }
        }

    }
}