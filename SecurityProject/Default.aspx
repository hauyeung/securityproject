﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="SecurityProject._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel runat="server" ID="LoggedInMessagePanel">
        <asp:Label runat="server" ID="WelcomeBackMessage"></asp:Label>
        <asp:HyperLink runat="server" ID="memberslink" Text="Members" NavigateUrl="~/Members/Default.aspx"></asp:HyperLink>
        <asp:HyperLink runat="server" ID="createuserlink" Text="Create User" NavigateUrl="~/Admin/CreateUser.aspx"></asp:HyperLink>
        <asp:HyperLink runat="server" ID="adminlink" Text="Manage Roles" NavigateUrl="~/Admin/ManageRoles.aspx"></asp:HyperLink>
        <asp:HyperLink runat="server" ID="HyperLink1" Text="Manage Users" NavigateUrl="~/Admin/ManageUsersWithRoles.aspx"></asp:HyperLink>
        <asp:HyperLink runat="server" ID="HyperLink2" Text="Delete Users" NavigateUrl="~/Admin/DeleteUser.aspx"></asp:HyperLink>
    </asp:Panel>
    <asp:Panel runat="Server" ID="NotLoggedInMessagePanel">
        <asp:HyperLink runat="server" ID="lnkLogin" Text="Log In" NavigateUrl="~/Account/Login.aspx"></asp:HyperLink>        
        <asp:HyperLink runat="server" ID="publiclink" Text="Public" NavigateUrl="~/Public/Default.aspx"></asp:HyperLink>        
    </asp:Panel>

    <h2>
        Welcome to ASP.NET!
    </h2>
    <p>
        To learn more about ASP.NET visit <a href="http://www.asp.net" title="ASP.NET Website">www.asp.net</a>.
    </p>
    <p>
        You can also find <a href="http://go.microsoft.com/fwlink/?LinkID=152368&amp;clcid=0x409"
            title="MSDN ASP.NET Docs">documentation on ASP.NET at MSDN</a>.
    </p>
</asp:Content>
